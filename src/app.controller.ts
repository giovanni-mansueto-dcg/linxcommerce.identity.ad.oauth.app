import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
  Param,
  Query,
  BadRequestException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { User } from './decorators/user.decorator';
import { MSALAuthGuard } from './auth/MSAL.guard';
import pca, { REDIRECT_URI } from './auth/AzureMSAL';
import { AdUser } from './auth/ad-user.entity';
import { selfRedirectUrl } from './auth/constants';
import {
  AuthService,
  cookieSettings,
  REDIRECT_COOKIE_NAME,
} from './auth/auth.service';
import { RedirectApp } from './redirect-app/redirect-app.entity';
import { RedirectAppService } from './redirect-app/redirect-app.service';

@Controller()
export class AppController {
  constructor(
    private readonly authService: AuthService,
    private readonly redirectAppService: RedirectAppService,
  ) {}

  async validateRedirectApp(id: string): Promise<boolean> {
    const entity = await this.redirectAppService.find(id, new RedirectApp());

    return !!entity;
  }

  @Get('status')
  getStatus(): any {
    return {
      ok: true,
    };
  }

  @Get('login/redirect')
  async loginRedirect(
    @Query('next') next,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<void> {
    const redirectAppId = req.cookies[REDIRECT_COOKIE_NAME];

    console.log({
      redirectAppId,
      next,
    });

    if (!redirectAppId || !next) {
      throw new BadRequestException();
    }

    const validateResult = await this.validateRedirectApp(redirectAppId);
    if (!validateResult) {
      throw new BadRequestException('Invalid RedirectApp');
    }

    const nextUrl = decodeURIComponent(next);

    if (!nextUrl.startsWith('https://login.microsoftonline.com')) {
      throw new BadRequestException('Invalid next URL');
    }

    res.redirect(nextUrl);
  }

  @Get(':redirectAppId/oauth')
  async oauth(
    @Param('redirectAppId') redirectAppId,
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<any> {
    if (!redirectAppId) {
      throw new BadRequestException();
    }

    const validateResult = await this.validateRedirectApp(redirectAppId);
    if (!validateResult) {
      throw new BadRequestException('Invalid RedirectApp');
    }

    const authCodeUrlParameters = {
      scopes: ['user.read', 'email'],
      redirectUri: REDIRECT_URI,
    };

    // get url to sign user in and consent to scopes needed for application
    return pca
      .getAuthCodeUrl(authCodeUrlParameters)
      .then((response: string) => {
        console.log('response login----', response);
        const selfBaseURL = selfRedirectUrl || '';
        const selfRedirectURL = `${selfBaseURL}?next=${encodeURIComponent(
          response,
        )}`;

        res.cookie(REDIRECT_COOKIE_NAME, redirectAppId, {
          ...cookieSettings(),
        });
        res.redirect(selfRedirectURL);
      })
      .catch((error) => {
        console.log(JSON.stringify(error));
        res.json({
          error: true,
        });
      });
  }

  @Get('redirect')
  async getRedirect(@Req() req: Request, @Res() res: Response): Promise<any> {
    // const tokenRequest = {
    //   code: req.query.code,
    //   scopes: ['user.read', 'email'],
    //   redirectUri: REDIRECT_URI,
    // };

    const redirectAppId = req.cookies[REDIRECT_COOKIE_NAME];

    const redirectApp = await this.redirectAppService.find(
      redirectAppId,
      new RedirectApp(),
    );
    if (!redirectApp) {
      throw new BadRequestException('Invalid RedirectApp');
    }

    const { url } = redirectApp;
    //const token = this.authService.createAccessToken(response);
    res.redirect(`${url}?code=${req.query.code}`);
  }

  @Post('oauth/access')
  async oauthAccess(@Req() req: Request, @Res() res: Response): Promise<any> {
    const tokenRequest = {
      code: req.body.code,
      scopes: ['user.read', 'email'],
      redirectUri: REDIRECT_URI,
    };

    const { appId, appSecret } = req.body;
    if (!appId || !appSecret) {
      throw new BadRequestException('Invalid RedirectApp');
    }

    const redirectApp = await this.redirectAppService.find(
      appId,
      new RedirectApp(),
    );
    if (!redirectApp || redirectApp.secret !== appSecret) {
      throw new BadRequestException('Invalid RedirectApp');
    }

    return pca
      .acquireTokenByCode(tokenRequest)
      .then(async (response) => {
        console.log('\nResponse: \n:', response);
        const { jwtExpiration, jwtToken } = await this.authService.createAccessToken(response);
        res.json({
          access_token: jwtToken,
          token_expiration: jwtExpiration,
        });

        // res.json({
        //   response,
        //   qs: req.query,
        //   appId,
        // });

        // return;
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send(error);
      });
  }

  @UseGuards(MSALAuthGuard)
  @Get('protected')
  protected(@User() user: AdUser): AdUser {
    return {
      ...user,
    };
  }
}
