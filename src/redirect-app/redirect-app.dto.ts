export class RedirectAppDTO {
  name: string;
  url: string;
  secret: string;
}
