import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UnprocessableEntityException,
  NotFoundException,
  Patch,
} from '@nestjs/common';
import { RedirectAppDTO } from './redirect-app.dto';
import { RedirectApp } from './redirect-app.entity';
import { RedirectAppService } from './redirect-app.service';

@Controller('redirect-apps')
export class RedirectAppController {
  constructor(private readonly redirectAppService: RedirectAppService) {}

  @Get()
  async getAllRedirectApps() {
    return await this.redirectAppService.findAll();
  }

  @Get(':rowKey')
  async getRedirectApp(@Param('rowKey') rowKey) {
    try {
      return await this.redirectAppService.find(rowKey, new RedirectApp());
    } catch (error) {
      // Entity not found
      throw new NotFoundException(error);
    }
  }

  @Post()
  async createRedirectApp(
    @Body()
    redirectAppData: RedirectAppDTO,
  ) {
    try {
      const redirectApp = new RedirectApp();
      // Disclaimer: Assign only the properties you are expecting!
      Object.assign(redirectApp, redirectAppData);

      return await this.redirectAppService.create(redirectApp);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Put(':rowKey')
  async saveRedirectApp(
    @Param('rowKey') rowKey,
    @Body() redirectAppData: RedirectAppDTO,
  ) {
    try {
      const originalRedirectApp = await this.redirectAppService.find(
        rowKey,
        new RedirectApp(),
      );
      const redirectApp = new RedirectApp();
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { secret, ...rest } = redirectAppData;
      Object.assign(redirectApp, {
        ...originalRedirectApp,
        ...rest,
      });

      return await this.redirectAppService.update(rowKey, redirectApp);
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }

  @Patch(':rowKey')
  async updateRedirectAppDetails(
    @Param('rowKey') rowKey,
    @Body() redirectAppData: Partial<RedirectAppDTO>,
  ) {
    try {
      const originalRedirectApp = await this.redirectAppService.find(
        rowKey,
        new RedirectApp(),
      );
      const redirectApp = new RedirectApp();
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { secret, ...rest } = redirectAppData;
      Object.assign(redirectApp, {
        ...originalRedirectApp,
        ...rest,
      });

      return await this.redirectAppService.update(
        rowKey,
        redirectApp as Partial<RedirectApp>,
      );
    } catch (error) {
      console.log('error', error);
      throw new UnprocessableEntityException(error);
    }
  }

  @Delete(':rowKey')
  async deleteDelete(@Param('rowKey') rowKey) {
    try {
      const response = await this.redirectAppService.delete(
        rowKey,
        new RedirectApp(),
      );

      if (response.statusCode === 204) {
        return null;
      } else {
        throw new UnprocessableEntityException(response);
      }
    } catch (error) {
      throw new UnprocessableEntityException(error);
    }
  }
}
