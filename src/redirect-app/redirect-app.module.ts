import { Module } from '@nestjs/common';
import { AzureTableStorageModule } from '@nestjs/azure-database';
import { RedirectAppController } from './redirect-app.controller';
import { RedirectAppService } from './redirect-app.service';
import { RedirectApp } from './redirect-app.entity';

@Module({
  imports: [
    AzureTableStorageModule.forFeature(RedirectApp, {
      table: process.env.AZURE_STORAGE_TABLE_NAME || 'idssoserver',
      createTableIfNotExists: true,
    }),
  ],
  providers: [RedirectAppService],
  controllers: [RedirectAppController],
  exports: [RedirectAppService],
})
export class RedirectAppModule {}
