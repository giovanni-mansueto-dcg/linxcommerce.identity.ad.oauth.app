import { v4 as uuid } from 'uuid';

import { Injectable } from '@nestjs/common';
import {
  Repository,
  InjectRepository,
  AzureTableStorageResultList,
  AzureTableStorageResponse,
} from '@nestjs/azure-database';
import { RedirectApp } from './redirect-app.entity';

@Injectable()
export class RedirectAppService {
  constructor(
    @InjectRepository(RedirectApp)
    private readonly redirectAppRepository: Repository<RedirectApp>,
  ) {}

  // find one redirectApp entitu by its rowKey
  async find(rowKey: string, redirectApp: RedirectApp): Promise<RedirectApp> {
    return this.redirectAppRepository.find(rowKey, redirectApp);
  }

  // find all redirectApp entities
  async findAll(): Promise<AzureTableStorageResultList<RedirectApp>> {
    return this.redirectAppRepository.findAll();
  }

  // create a new redirectApp entity
  async create(redirectApp: RedirectApp): Promise<RedirectApp> {
    redirectApp.secret = uuid();
    return this.redirectAppRepository.create(redirectApp);
  }

  // update the a redirectApp entity by its rowKey
  async update(
    rowKey: string,
    redirectApp: Partial<RedirectApp>,
  ): Promise<RedirectApp> {
    return this.redirectAppRepository.update(rowKey, redirectApp);
  }

  // delete a redirectApp entity by its rowKey
  async delete(
    rowKey: string,
    redirectApp: RedirectApp,
  ): Promise<AzureTableStorageResponse> {
    return this.redirectAppRepository.delete(rowKey, redirectApp);
  }
}
