import { Test, TestingModule } from '@nestjs/testing';
import { RedirectAppService } from './redirect-app.service';

describe('RedirectAppService', () => {
  let service: RedirectAppService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RedirectAppService],
    }).compile();

    service = module.get<RedirectAppService>(RedirectAppService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
