import {
  EntityPartitionKey,
  EntityRowKey,
  EntityString,
} from '@nestjs/azure-database';

@EntityPartitionKey('RedirectAppID')
@EntityRowKey('RedirectAppName')
export class RedirectApp {
  @EntityString() name: string;
  @EntityString() url: string;
  @EntityString() secret: string;
}
