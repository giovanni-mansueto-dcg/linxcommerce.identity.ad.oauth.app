import { Test, TestingModule } from '@nestjs/testing';
import { RedirectAppController } from './redirect-app.controller';

describe('RedirectAppController', () => {
  let controller: RedirectAppController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RedirectAppController],
    }).compile();

    controller = module.get<RedirectAppController>(RedirectAppController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
