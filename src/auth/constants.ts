import fetch from 'node-fetch';

/*
  openssl genrsa -out private.pem 2048
  openssl rsa -in private.pem -pubout -out public.pem
*/
let privateCachedData: Buffer = null;

export const getPrivateKey = async () => {
  if (privateCachedData !== null) {
    return privateCachedData;
  }

  if (!process.env.APP_PRIVATE_KEY) {
    console.error('APP_PRIVATE_KEY not defined');
    return null;
  }

  const response = await fetch(process.env.APP_PRIVATE_KEY);
  const pem = await response.buffer();
  privateCachedData = pem;

  return pem;
};

export const jwtSettings = {
  expirationTime: process.env.APP_TOKEN_EXPIRATION || '90d',
};

export const appUrl = process.env.APP_HOST;

export const selfRedirectUrl = `${appUrl}/login/redirect`;
