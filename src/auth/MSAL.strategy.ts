import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { BearerStrategy } from 'passport-azure-ad';
import { REDIRECT_URI } from './AzureMSAL';

export const passportStrategyName = 'oauth-bearer';

@Injectable()
export class AzureADStrategy extends PassportStrategy(
  BearerStrategy,
  passportStrategyName,
) {
  constructor() {
    super({
      identityMetadata: `https://login.microsoftonline.com/${process.env.AZURE_TENANTID}/.well-known/openid-configuration`,
      clientID: process.env.AZURE_CLIENTID,
      clientSecret: process.env.AZURE_CLIENTSECRET,
      redirectUrl: REDIRECT_URI,
      responseType: 'id_token',
      responseMode: 'query',
      validateIssuer: false,
      loggingLevel: 'error', //'info',
    });
  }

  async validate(response: any) {
    //debugger;
    console.log('validated response', response);
    //const { unique_name }: { unique_name: string } = response;
    return response;
  }
}
