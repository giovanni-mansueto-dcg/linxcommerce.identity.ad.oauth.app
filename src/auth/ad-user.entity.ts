export class AdUser {
  name: string;
  email: string;
  url: string;
  aud: string;
  iss: string;
  iat: number;
  nbf: number;
  exp: number;
  roles: Array<string>;
  aio: string;
  oid: string;
  preferred_username: string;
  rh: string;
  sub: string;
  tid: string;
  uti: string;
  ver: string;
}
