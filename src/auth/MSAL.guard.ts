import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { passportStrategyName } from './MSAL.strategy';

@Injectable()
export class MSALAuthGuard extends AuthGuard(passportStrategyName) {}
