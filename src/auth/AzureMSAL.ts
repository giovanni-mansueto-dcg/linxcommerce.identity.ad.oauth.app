import { appUrl } from './constants';

/* eslint-disable @typescript-eslint/no-var-requires */
const msal = require('@azure/msal-node');

const port = process.env.PORT || 3000;

const isDev = process.env.NODE_ENV === 'development';
export const REDIRECT_URI = isDev
  ? `http://localhost:${port}/redirect`
  : `${appUrl}/redirect`;

// Before running the sample, you will need to replace the values in the config,
// including the clientSecret
const config = {
  auth: {
    clientId: process.env.AZURE_CLIENTID,
    authority: process.env.AZURE_AUTHORITY || 'https://login.microsoftonline.com/common',
    clientSecret: process.env.AZURE_CLIENTSECRET,
  },
  system: {
    loggerOptions: {
      loggerCallback(loglevel, message, containsPii) {
        console.log(loglevel, message, containsPii);
      },
      piiLoggingEnabled: false,
      logLevel: msal.LogLevel.Verbose,
    },
  },
};

// Create msal application object
const pca = new msal.ConfidentialClientApplication(config);
export default pca;
