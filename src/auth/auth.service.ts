import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { getPrivateKey } from './constants';

interface AuthResponseClaims {
  adData: any;
  id: string; // ad ID - oid
  name: string;
  email: string;
  roles: Array<string>;
}

interface AccessTokenResponse {
  jwtExpiration: number;
  jwtToken: string;
}

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  async createAccessToken(msalResponse: any): Promise<AccessTokenResponse> {
    const {
      idTokenClaims = {},
      account,
      idToken,
      accessToken,
      expiresOn,
    } = msalResponse;

    const adData = {
      account,
      idToken,
      accessToken,
      expiresOn,
    };
    const payload: AuthResponseClaims = {
      adData,
      name: idTokenClaims.name,
      id: idTokenClaims.oid,
      email: idTokenClaims.email,
      roles: idTokenClaims.roles,
    };

    const pem = await getPrivateKey();
    const jwtToken = this.jwtService.sign(payload);

    const token = await this.jwtService.verifyAsync(jwtToken, {
      publicKey: pem,
    });
    // const separetor = baseUrl.indexOf('?') !== -1 ? '&' : '?';

    return {
      jwtToken,
      jwtExpiration: token.exp as number,
    };
    //return `${baseUrl}${separetor}access_token=${accessToken}`;
  }
}

export const cookieSettings = () => {
  const domain =
    process.env.NODE_ENV === 'development' ? 'localhost' : undefined;

  const date = new Date();
  const minutes = 5;
  date.setTime(date.getTime() + minutes * 60000);

  return {
    httpOnly: true,
    expires: date,
    domain,
  };
};

export const REDIRECT_COOKIE_NAME = 'redirectAppID';
