import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { getPrivateKey } from './constants';
import { JwtStrategy } from './jwt.strategy';
import { AuthService } from './auth.service';
import { AzureADStrategy } from './MSAL.strategy';
import { jwtSettings } from './constants';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useFactory: async () => {
        const pem = await getPrivateKey();
        const algorithm = 'RS256';
        return {
          privateKey: pem,
          signOptions: {
            expiresIn: jwtSettings.expirationTime,
            algorithm: algorithm,
          },
          verifyOptions: { algorithms: [algorithm] },
        };
      },
    }),
  ],
  providers: [AuthService, JwtStrategy, AzureADStrategy],
  exports: [AuthService],
})
export class AuthModule {}
