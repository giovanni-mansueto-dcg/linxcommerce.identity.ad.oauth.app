import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RedirectAppModule } from './redirect-app/redirect-app.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [AuthModule, RedirectAppModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
